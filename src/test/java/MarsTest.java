import static org.junit.Assert.*;

import org.junit.Test;

import marsRover.Mars;

public class MarsTest {

	@Test
	public void worldSize() {
		String size = "5, 5";
		Integer xSize = 5;
		Integer ySize = 5;
		Mars mars = new Mars(size);
		
		Integer action = mars.getXSize();
		
		assertEquals(xSize, action);
		assertEquals(ySize, action);
	}
}
