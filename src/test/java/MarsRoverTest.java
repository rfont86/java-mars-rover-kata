import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import marsRover.Rover;
import marsRover.Mars;

public class MarsRoverTest {
	Rover rover;
	Mars mars;
	String size = "5, 5";
	
	@Before
	public void initialize() {
		mars = new Mars(size);
		rover = new Rover(mars);
	}

	@Test
	public void greenBelt() {
		String landingPosition = "5, 5, N";
		String finalPosition = "5, 5, N";
		rover.landing(landingPosition);
		
		rover.command("");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);
	}
	
	@Test
	public void yellowBeltMoveForward() {
		String landingPosition = "1, 2, N";
		String finalPosition = "1, 5, N";
		rover.landing(landingPosition);
		
		rover.command("M, M, M");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void yellowBeltTurnDirection() {
		String landingPosition = "1, 2, N";
		String finalPosition = "1, 5, E";
		rover.landing(landingPosition);
		
		rover.command("M, M, M, R");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void yellowBeltRandomMoves() {
		String landingPosition = "1, 1, N";
		String finalPosition = "4, 3, W";
		rover.landing(landingPosition);
		
		rover.command("M, R, M, M, M, L, M, L");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void redBeltAroundWorldByNorth() {
		String landingPosition = "5, 5, N";
		String finalPosition = "5, 1, N";
		rover.landing(landingPosition);
		
		rover.command("M");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void redBeltAroundWorldByEast() {
		String landingPosition = "5, 5, E";
		String finalPosition = "1, 5, E";
		rover.landing(landingPosition);
		
		rover.command("M");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void redBeltAroundWorldBySouth() {
		String landingPosition = "1, 1, S";
		String finalPosition = "1, 5, S";
		rover.landing(landingPosition);
		
		rover.command("M");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
	@Test
	public void redBeltAroundWorldByWest() {
		String landingPosition = "1, 1, W";
		String finalPosition = "5, 1, W";
		rover.landing(landingPosition);
		
		rover.command("M");
		String newPosition = rover.getPosition();
		
		assertEquals(finalPosition, newPosition);	
	}
	
}