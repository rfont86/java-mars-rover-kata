package marsRover;

import java.util.ArrayList;
import java.util.HashMap;

import marsRover.commands.ICommand;
import marsRover.commands.Left;
import marsRover.commands.Move;
import marsRover.commands.Right;
import marsRover.directions.Direction;
import marsRover.inputArrangement.CommandArrangement;
import marsRover.inputArrangement.InputArrangement;

public class Rover {
	
	private final int X_POINT = 0;
	private final int Y_POINT = 1;
	
	private Direction direction;
	private int[] position = new int[2];
	
	private Mars mars;
	
	private HashMap <String, ICommand> keyCommands = new HashMap<>();
	
	public Rover(Mars mars) {
		this.mars = mars;
		
		this.keyCommands.put("M", new Move());
		this.keyCommands.put("R", new Right());
		this.keyCommands.put("L", new Left());
	}

	public void landing(String landingPosition) {
		InputArrangement inputArrangement = new InputArrangement();
		inputArrangement.toParse(landingPosition);
		
		this.position = inputArrangement.getPoint();
		this.direction = inputArrangement.getDirection();
	}
	
	public void command(String direction) {
		CommandArrangement commandArrangement = new CommandArrangement(direction);
		ArrayList<String> commands = commandArrangement.getCommands();
		commands.forEach(command -> this.execute(command));
	}

	public String getPosition() {	
		int xCoordenate = this.position[X_POINT];
		int yCoordenate = this.position[Y_POINT];
		String direction = this.direction.getName();
		String formattedPosition = (xCoordenate + ", " + yCoordenate + ", " + direction);
		
		return formattedPosition;
	}
	
	private void execute(String command) {
		this.position = this.keyCommands.get(command).execute(this.direction, this.position);
		this.position = mars.aroundTheWorld(this.position);
		this.direction = this.keyCommands.get(command).turn(this.direction);
	}
	
}









