package marsRover.commands;

import marsRover.directions.Direction;

public interface ICommand {

	int[] execute(Direction direction, int[] position);
	
	Direction turn(Direction direction);
}
