package marsRover.commands;

import marsRover.directions.Direction;

public class Move implements ICommand{
	
	public int[] execute(Direction direction, int[] position) {
		return direction.move(position);
	}

	public Direction turn(Direction direction) {
		return direction;
	}

}
