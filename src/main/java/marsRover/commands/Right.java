package marsRover.commands;

import marsRover.directions.Direction;

public class Right implements ICommand {

	public int[] execute(Direction direction, int[] position) {
		return position;
	}

	public Direction turn(Direction direction) {
		return direction.right();
	}

}
