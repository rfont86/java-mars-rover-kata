package marsRover;

import marsRover.inputArrangement.InputArrangement;

public class Mars {
	
	private final int START_POSITION = 1;
	private final int OUT_OF_WORLD = 0;
	
	private final int X_POINT = 0;
	private final int Y_POINT = 1;

	private int[] size = new int[2];
	int[] currentPosition = new int[2];

	public Mars(String size) {
		InputArrangement inputArrangement = new InputArrangement();
		inputArrangement.toParse(size);
		
		this.size = inputArrangement.getPoint();
	}
	
	public int getXSize() {
		return this.size[X_POINT];
	}
	
	public int getYSize() {
		return this.size[Y_POINT];
	}
	
	public int[] aroundTheWorld(int[] position) {	
		this.currentPosition[X_POINT] = this.aroundOneDimensionWorld(position[X_POINT], this.getXSize());
		this.currentPosition[Y_POINT] = this.aroundOneDimensionWorld(position[Y_POINT], this.getYSize());
		
		return this.currentPosition;
	}
	
	private int aroundOneDimensionWorld(int point, int sizeOneDimension) {
		int lastPosition = sizeOneDimension;
		int currentPoint = point;
		if(point > lastPosition) currentPoint = START_POSITION;
		if(point == OUT_OF_WORLD) currentPoint = lastPosition;
		
		return currentPoint;
	}
}
