package marsRover.directions;

public class West implements Direction {

	public int[] move(int[] position2) {
		Direction.location[0] = -1 + position2[0];
		Direction.location[1] = 0 + position2[1];
		return Direction.location;
	}

	public Direction right() {
		return new North();
	}

	public Direction left() {
		return new South();
	}

	public String getName() {
		return "W";
	}
}
