package marsRover.directions;

public class North implements Direction {
	
	public int[] move(int[] position) {	
		Direction.location[0] = 0 + position[0];
		Direction.location[1] = 1 + position[1];
		
		return Direction.location;
	}

	public Direction right() {
		return new East();
	}

	public Direction left() {
		return new West();
	}

	public String getName() {
		return "N";
	}
	
}
