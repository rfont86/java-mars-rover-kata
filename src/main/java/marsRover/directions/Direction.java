package marsRover.directions;

public interface Direction {
	
	public static final int[] location = new int[2];

	int[] move(int[] position);
	
	Direction right();

	Direction left();

	String getName();
}
