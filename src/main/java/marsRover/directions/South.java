package marsRover.directions;

public class South implements Direction {

	public int[] move(int[] position2) {
		Direction.location[0] = 0 + position2[0];
		Direction.location[1] = -1 + position2[1];
		return Direction.location;
	}

	public Direction right() {
		return new West();
	}

	public Direction left() {
		return new East();
	}
	
	public String getName() {
		return "S";
	}
}
