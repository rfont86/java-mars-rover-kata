package marsRover.directions;

public class East implements Direction {
	
	public int[] move(int[] position) {
		Direction.location[0] = 1 + position[0];
		Direction.location[1] = 0 + position[1];
		return Direction.location;
	}

	public Direction right() {
		return new South();
	}

	public Direction left() {
		return new North();
	}
	
	public String getName() {
		return "E";
	}

}
