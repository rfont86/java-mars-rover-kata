package marsRover.inputArrangement;

import java.util.ArrayList;

public class CommandArrangement {
	
	private ArrayList<String> commands = new ArrayList<String>();
	
	public CommandArrangement(String direction) {
		for(int i = 0; i < direction.length(); i++) {
			boolean isACommand = Character.isLetter(direction.charAt(i));
			if(isACommand) {
				if(direction.charAt(i) == 'M'){
					this.commands.add("M");
				}else if(direction.charAt(i) == 'R'){
					this.commands.add("R");
				}else if(direction.charAt(i) == 'L'){
					this.commands.add("L");
				}
			}
		}
	}
	
	public ArrayList<String> getCommands(){
		return this.commands;
	}
}	
