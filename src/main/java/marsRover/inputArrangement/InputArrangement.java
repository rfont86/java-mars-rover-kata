package marsRover.inputArrangement;

import java.util.HashMap;

import marsRover.directions.Direction;
import marsRover.directions.East;
import marsRover.directions.North;
import marsRover.directions.South;
import marsRover.directions.West;

public class InputArrangement {
	
	private final int REST_TO_CAST_WELL = 48;
	
	private Direction direction;
	private int[] position = new int[2];
	
	HashMap <Character, Direction> keyDirection = new HashMap<>();
	
	public InputArrangement() {
		keyDirection.put('N', new North());
		keyDirection.put('E', new East());
		keyDirection.put('S', new South());
		keyDirection.put('W', new West());
	}
	
	public void toParse(String landingPosition) {
		for(int i = 0; i < landingPosition.length(); i++) {
			char coordinate = landingPosition.charAt(i);
			
			if(Character.isDigit(coordinate)) {
				this.savePositions(coordinate);
			}else if(Character.isLetter(coordinate)) {
				this.direction = this.keyDirection.get(coordinate);
			}
		}
	}

	public int[] getPoint() {
		return this.position;
	}
	
	public Direction getDirection() {
		return this.direction;
	}
	
	private void savePositions(char coordinate) {
		if(this.positionIsEmpty()) {
			this.position[0] = ((int) coordinate) - REST_TO_CAST_WELL;
		}else{
			this.position[1] = ((int) coordinate) - REST_TO_CAST_WELL;	
		}
	}
	
	private boolean positionIsEmpty() {
		return this.position[0] == 0;
	}
	
}
